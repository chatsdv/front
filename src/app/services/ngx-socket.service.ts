import { Injectable } from "@angular/core";
import { Socket, SocketIoConfig } from "ngx-socket-io";

const config: SocketIoConfig = { url: "http://localhost:3000", options: {} };

@Injectable({
  providedIn: "root",
})
export class NgxSocketService extends Socket {
  constructor() {
    super(config);
    this.disconnect();
  }
}
