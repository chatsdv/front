import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environment/environment';
import { ChatMessage } from '../models/chatMessage';
import { Socket } from 'ngx-socket-io';
import { NgxSocketService } from "./ngx-socket.service";


@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private socket = inject(NgxSocketService);


  private apiUrl = environment.url;

  currentMessage = this.socket.fromEvent<ChatMessage>('message');
  messages = this.socket.fromEvent<ChatMessage[]>('messages');

  constructor() {}

  getMessages() {
    this.socket.emit('getMsgs');
  }

  newMessage(chatMessage: ChatMessage) {
    this.socket.emit('addMsg', chatMessage);
  }


  /*
  getMessages(): Observable<ChatMessage[]> {
    return this.http.get<ChatMessage[]>(`${this.apiUrl}/messages/`);
  }

  createMessage(chatMessage: ChatMessage): Observable<ChatMessage> {
    return this.http.post<ChatMessage>(`${this.apiUrl}/messages/`, chatMessage, {

    });
  }*/

}

