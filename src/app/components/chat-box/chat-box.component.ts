import { Component, OnDestroy } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { FormControl,FormGroup, FormBuilder, Validators } from  '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ChatMessage } from '../../models/chatMessage';
import { MessagesService } from '../../services/message.service';
import { Observable, Subscription , tap } from 'rxjs';

@Component({
  selector: 'app-chat-box',
  standalone: true,
  imports: [CommonModule,MatInputModule,MatListModule,MatIconModule,MatButtonModule,MatCardModule,ReactiveFormsModule],
  templateUrl: './chat-box.component.html',
  styleUrl: './chat-box.component.scss',
  providers: [MessagesService]
})

export class ChatBoxComponent {

  messages!: Observable<ChatMessage[]>;
  currentMessage!: ChatMessage;
  commentaireForm = new FormGroup({
    message: new FormControl(""),
  });

  private _msgSub!: Subscription;

  constructor( private messagesService: MessagesService) {
  }

  ngOnInit(): void {
    this.messages = this.messagesService.messages;
    this._msgSub = this.messagesService.currentMessage.subscribe(msg => this.currentMessage = msg);
    this.messagesService.getMessages();
    //this.messages.push({ content: 'Bienvenue dans le chatBox', sender: "Victor" });

    /*this.messagesService.getMessages().pipe(
        tap({
          next: (newMessage) => {
            this.messages = newMessage;
          },
          error: (error) => {
            console.error('Une erreur s\'est produite : ', error);
          }
        })
      ).subscribe();
    */
  }

  ngOnDestroy() {
    this._msgSub.unsubscribe();
  }

  onSubmit() {
    const newCom = this.commentaireForm.get('message')?.value;

    const newMessage: ChatMessage = { content: String(newCom), sender: "Victor" };
    this.messagesService.newMessage(newMessage);

    this.commentaireForm.setValue({
      message:"",
    });

  }

  reloadChatBox(): void {
    this.messagesService.getMessages();
  }

}
