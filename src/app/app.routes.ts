import { Routes } from '@angular/router';
import { ChatBoxComponent } from './components/chat-box/chat-box.component';

export const routes: Routes = [{
    path: '',
    component: ChatBoxComponent,
  },];
